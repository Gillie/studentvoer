<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Step;
use App\Recipe;

class StepController extends Controller
{
    public function add($id) {
    	return view('step.add', compact('id'));
    }

    public function store(Request $request, Step $step, $id) {
    	$this->validate($request, [
    		'step' => 'string|required'
    	]);

    	$step = new Step;
    	$step->step = $request->step;
    	$step->recipe_id = $id;
    	$step->save();
    	\Session::flash('success', 'De stap is succesvol toegevoegd');
    	return redirect()->action('RecipeController@index');
    }

    public function edit($id, Step $step) {
    	return view('step.edit', compact('step', 'id'));
    }

    public function update(Request $request, $id, Step $step) {
    	$this->validate($request, [
    		'step' => 'string|required'
    	]);

    	$step->update($request->all());
    	\Session::flash('success', 'De stap is succesvol gewijzigd');
    	return redirect()->action('RecipeController@index');
    }

    public function delete($id, Step $step) {
    	$step->delete();
    	\Session::flash('success', 'De stap is succesvol verwijderd');
    	return back();
    }
}
