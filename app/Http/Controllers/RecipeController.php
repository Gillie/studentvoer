<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Recipe;
use App\User;
use App\Step;

class RecipeController extends Controller
{
    public function index() {
    	$recipes = Recipe::has('user')->get();
    	$deletedRecipes = Recipe::onlyTrashed()->get();

    	return view('recipe.index', compact('recipes', 'deletedRecipes'));
    }

    public function add() {
    	$users = User::all();
    	return view('recipe.add', compact('users'));
    }

    public function store(Request $request, Recipe $recipe) {
    	$this->validate($request, [
    		'name' => 'string|min:1|max:80|required',
    		'description' => 'string|required',
    		'ingredients' => 'string|required',
    		'user_id' => 'numeric|exists:users,id|required'
    	]);
    	//dd($request->user_id);
    	$recipe = new Recipe;
    	$recipe->name = $request->name;
    	$recipe->description = $request->description;
    	$recipe->ingredients = $request->ingredients;
    	$recipe->user_id = $request->user_id;
    	$recipe->save();
    	\Session::flash('success', 'Het recept: ' . $request->name . ' is succesvol toegevoegd');
    	return redirect()->action('RecipeController@index');
    }

    public function edit(Recipe $recipe) {
    	$users = User::all();
    	return view('recipe.edit', compact('recipe', 'users'));
    }

    public function update(Request $request, Recipe $recipe) {
    	$this->validate($request, [
    		'name' => 'string|min:1|max:80|required',
    		'description' => 'string|required',
    		'ingredients' => 'string|required',
    		'user_id' => 'numeric|exists:users,id|required'
    	]);

    	$recipe->name = $request->name;
    	$recipe->description = $request->description;
    	$recipe->ingredients = $request->ingredients;
    	$recipe->user_id = $request->user_id;
    	$recipe->save();
    	\Session::flash('success', 'Het recept: ' . $request->name . ' is succesvol gewijzigd');
    	return redirect()->action('RecipeController@index');
    }

    public function delete(Recipe $recipe) {
    	$recipe->delete();
    	\Session::flash('success', 'Het recept: ' . $recipe->name . ' is succesvol verwijderd');
    	return back();
    }

    public function restore($id) {
    	$recipe = Recipe::onlyTrashed()->find($id);
    	$recipe->restore();
    	\Session::flash('success', 'Het recept: ' . $recipe->name . ' is succesvol hersteld');
    	return back();
    }

    public function show(Recipe $recipe) {
    	$steps = Step::has('recipe')->oldest()->first();
    	$getal = 1;
    	return view('recipe.show', compact('recipe', 'steps', 'getal'));
    }
}
