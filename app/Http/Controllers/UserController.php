<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
   	public function index() {
   		$users = User::all();
   		$deletedUsers = User::onlyTrashed()->get();

   		return view('user.index', compact('users', 'deletedUsers'));
   	}

   	public function edit(User $user) {
   		return view('user.edit', compact('user'));
   	}

   	public function update(Request $request, User $user) {
   		$date = date('Y-m-d');

   		$this->validate($request, [
   			'firstname' => 'string|min:1|max:45|required',
   			'lastname' => 'string|min:1|max:45|required',
   			'geboortedatum' => 'date|before:' . $date . '|required',
   			'email' => 'email|unique:users,email,' . $user->id
   		]);
   		$user->update($request->all());
   		\Session::flash('success', 'De gebruiker: ' .  $request->firstname . ' is succesvol gewijzigd.');
   		return redirect()->action('UserController@index');
   	}

   	public function delete(User $user) {
   		$user->delete();
   		\Session::flash('success', 'De gebruiker: ' . $user->firstname . ' is succesvol verwijderd.');
   		return back();
   	}

   	public function restore($id) {
   		$user = User::onlyTrashed()->find($id);
   		$user->restore();
   		\Session::flash('success', 'De gebruiker: ' . $user->firstname . ' is succesvol hersteld');
   		return back();
   	}
}
