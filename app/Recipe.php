<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;

    protected $table = 'recipes';

    public $timestamps = true;

     protected $fillable = [
        'name',
        'description',
        'ingredients',
        'users_id'
    ];

    protected $guarded = [];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function comment() {
    	return $this->hasMany('App\Comment');
    }

    public function step() {
    	return $this->hasMany('App\Step');
    }
}
