<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use SoftDeletes;

    protected $table = 'comments';

    public $timestamps = true;

     protected $fillable = [
        'comment'
    ];

    protected $guarded = [];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function recipe() {
    	return $this->belongsTo('App\Recipe');
    }
}
