<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Step extends Model
{
    use SoftDeletes;

    protected $table = 'steps';

    public $timestamps = true;

     protected $fillable = [
        'step'
    ];

    protected $guarded = [];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at' ,
    ];

    public function recipe() {
    	return $this->belongsTo('App\Recipe');
    }
}