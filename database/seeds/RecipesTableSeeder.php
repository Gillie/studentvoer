<?php

use Illuminate\Database\Seeder;
use App\Recipe;

class RecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Recipe::create(array(
        	'name' => 'Bamisoep',
            'description' => 'Geniet van een overheelijke soep die je ook lekker opwarmt.',
            'ingredients' => 'Noodles, Mini Friet(Naturel), Gebakken uitjes, selderij',
        	'users_id' => 1
        ));

        Recipe::create(array(
        	'name' => 'Zuurkool',
            'description' => 'Een mooi gevuld bordje met rijst en zuurkool.',
            'ingredients' => 'Zuurkool(300 gram), Rijst(200 gram), Spekreepjes, Aardappelen',
        	'users_id' => 2
        ));
    }
}
