<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
        	'firstname' => 'Gilliano',
        	'lastname' => 'Herkul',
        	'geboortedatum' => '1997-05-15',
            'rol' => 'admin',
        	'email' => 'gilliano_herkul@live.nl',
        	'password' => bcrypt('Welkom123')
        ));

        User::create(array(
        	'firstname' => 'Arsenio',
        	'lastname' => 'Panhuijsen',
        	'geboortedatum' => '1998-10-10',
            'rol' => 'student',
        	'email' => 'arsenio_panhuijsen@live.nl',
        	'password' => bcrypt('Welkom')
        ));
    }
}
