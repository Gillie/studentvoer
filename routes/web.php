<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

//Routes for users
Route::get('/', 'PagesController@home');
Route::get('/users', 'UserController@index');
Route::get('/users/{user}/edit', 'UserController@edit');
Route::patch('/users/{user}/update', 'UserController@update');
Route::get('/users/{user}/delete', 'UserController@delete');
Route::get('/users/{user}/restore', 'UserController@restore');

//Routes for recipes
Route::get('/recipes', 'RecipeController@index');
Route::get('/recipes/add', 'RecipeController@add');
Route::post('/recipes/add', 'RecipeController@store');
Route::get('/recipes/{recipe}/edit', 'RecipeController@edit');
Route::patch('/recipes/{recipe}/update', 'RecipeController@update');
Route::get('/recipes/{recipe}/delete', 'RecipeController@delete');
Route::get('/recipes/{recipe}/restore', 'RecipeController@restore');
Route::get('/recipes/{recipe}', 'RecipeController@show');

//Routes for steps
Route::get('/recipes/{recipe}/steps/add', 'StepController@add');
Route::post('/recipes/{recipe}/steps/add', 'StepController@store');
Route::get('/recipes/{recipe}/steps/{step}/edit', 'StepController@edit');
Route::patch('/recipes/{recipe}/steps/{step}/update', 'StepController@update');
Route::get('/recipes/{recipe}/steps/{step}/delete', 'StepController@delete');

