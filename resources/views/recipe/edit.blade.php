@extends('welcome')

@section('content')
@include('/errors._form_errors')
	<div class="col-md-9 col-md-offset-1">
		<form method="post" action="/recipes/{{ $recipe->id }}/update">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group">
		  			<input type="text" class="form-control" name="name" value="{{ $recipe->name }}" minlength="1" maxlength="80" required/>
		  		</div>
		  		<div class="form-group">
		  			<select name="user_id" class="form-control">
		  				@foreach ($users as $user)
		  					@if ($user->id === $recipe->user_id)
		  						<option value="{{ $user->id }}" selected>{{ $user->firstname . " " . $user->lastname }}</option>
		  					@else 
		  						<option value="{{ $user->id }}">{{ $user->firstname . " " . $user->lastname }}</option>
		  					@endif
		  				@endforeach
		  			</select>
		  		</div>
		  		<div class="form-group">
		  			<textarea name="description" class="form-control" rows="4">{{ $recipe->description }}</textarea>
		  		</div>
		  		<div class="form-group">
		  			<textarea name="ingredients" class="form-control" rows="4">{{ $recipe->ingredients }}</textarea>
		  		</div>

			  	<div class="form-group">
			  		<button type="submit" class="btn btn-success form-control">Wijzigen</button>
			  	</div>
			</form>
	</div>
@stop