@extends('welcome')

@section('content')
	<div class="col-md-12">
		<div class="page-header"><h3><b>{{ $recipe->name }}</b></h3></div>
		<div class="row">
			<div class="col-md-4">
				<b>Beschrijving</b> <br> <br>
				{{ $recipe->description }}	
			</div>
			<div class="col-md-4">
				<b>Ingredienten</b> <br> <br>
				{{ $recipe->ingredients }}
			</div>
			<div class="col-md-4">
				<b>Gemaakt door:</b> <br> <br>
				{{ $recipe->user->firstname . " " . $recipe->user->lastname }}
			</div>
		</div>
		<div class="row">
			<br>
			<br>
			<br>
		</div>
		<h4>Stappen</h4>
		<br>
		<a href="/recipes/{{ $recipe->id }}/steps/add"><button type="submit" class="btn btn-success">Voeg stappen toe</button></a>
		<div class="row">
			<br>
		</div>
		<table class="table table-striped">
			@foreach ($recipe->step as $step)
				<tr>
					<td>{{ $getal . ".   " . $step->step }}</td>
					<td>
						<a href="/recipes/{{ $recipe->id }}/steps/{{ $step->id }}/edit"><button type="submit" class="btn btn-primary">Wijzigen</button></a>
					</td>
					<td>
						<a href="/recipes/{{ $recipe->id }}/steps/{{ $step->id }}/delete"><button type="submit" class="btn btn-danger">Verwijderen</button></a>
					</td>
				</tr>
				<?php $getal++ ?>
			@endforeach
		</table>
	</div>
@stop