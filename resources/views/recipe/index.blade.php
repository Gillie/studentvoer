@extends('welcome')

@section('content')
	<div class="col-md-12">
		<div class="page-header"><h3><b>Recepten</b></h3></div>
		<a href="/recipes/add"><button type="submit" class="btn btn-success">Toevoegen</button></a>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Naam recept</th>
						<th>Beschrijving</th>
						<th>Ingredienten</th>
						<th>Naam student</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($recipes as $recipe)
						<tr>
							<td>{{ $recipe->name }}</td>
							<td>{{ $recipe->description }}</td>
							<td>{{ $recipe->ingredients }}</td>
							<td>{{ $recipe->user->firstname }}</td>
							<td><a href="/recipes/{{ $recipe->id }}/edit"><button type="button" class="btn btn-primary">Wijzigen</button></a></td>
							<td>
							<a href="/recipes/{{ $recipe->id }}/delete"><button type="button" class="btn btn-danger">Verwijderen</button></a>
							<a href="/recipes/{{ $recipe->id }}"><button type="button" class="btn btn-info">Info</button></a>
						</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			
			<hr>
			@if (!$deletedRecipes->isEmpty())
			<div class="page-header"><h3><b>Verwijderde recepten</b></h3></div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Naam recept</th>
						<th>Beschrijving</th>
						<th>Ingredienten</th>
						<th>Naam student</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($deletedRecipes as $recipe)
						<tr>
							<td>{{ $recipe->name }}</td>
							<td>{{ $recipe->description }}</td>
							<td>{{ $recipe->ingredients }}</td>
							<td>{{ $recipe->user->firstname }}</td>
						<td>
							<a href="/recipes/{{ $recipe->id }}/restore"><button type="button" class="btn btn-success">Herstellen</button></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
@stop