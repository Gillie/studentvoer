@extends('welcome')

@section('content')
@include('/errors._form_errors')
	<div class="col-md-9 col-md-offset-1">
		<form method="post" action="/recipes/add">
				{{ csrf_field() }}
				<div class="form-group">
		  			<input type="text" class="form-control" name="name" placeholder="Naam recept" value="{{ old('name') }}" minlength="1" maxlength="80" required/>
		  		</div>
		  		<div class="form-group">
		  			<select name="user_id" class="form-control">
		  				@foreach ($users as $user)
		  					<option value="{{ $user->id }}">{{ $user->firstname . " " . $user->lastname }}</option>
		  				@endforeach
		  			</select>
		  		</div>
		  		<div class="form-group">
		  			<textarea name="description" class="form-control" placeholder="Beschrijving" rows="4">{{ old('description') }}</textarea>
		  		</div>
			  	<div class="form-group">
		  			<textarea name="ingredients" class="form-control" placeholder="Ingredienten" rows="4">{{ old('ingredients') }}</textarea>
		  		</div>
			  	
			  	<div class="form-group">
			  		<button type="submit" class="btn btn-success form-control">Toevoegen</button>
			  	</div>
			</form>
	</div>
@stop