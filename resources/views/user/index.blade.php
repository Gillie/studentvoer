@extends('welcome')

@section('content')
	<div class="col-md-10 col-md-offset-1">
		<div class="page-header"><h3><b>Gebruikers</b></h3></div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Voornaam</th>
						<th>Achternaam</th>
						<th>Geboortedatum</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $user)
						<tr>
							<td>{{ $user->firstname }}</td>
							<td>{{ $user->lastname }}</td>
							<td>{{ \Carbon\Carbon::parse($user->geboortedatum)->format('d-m-Y') }}</td>
							<td>{{ $user->email }}</td>
							<td><a href="/users/{{ $user->id }}/edit"><button type="button" class="btn btn-primary">Wijzigen</button></a></td>
							<td>
							<a href="/users/{{ $user->id }}/delete"><button type="button" class="btn btn-danger">Verwijderen</button></a>
						</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			
			<hr>
			@if (!$deletedUsers->isEmpty())
			<div class="page-header"><h3><b>Verwijderde gebruikers</b></h3></div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Voornaam</th>
						<th>Achternaam</th>
						<th>Geboortedatum</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($deletedUsers as $user)
						<tr>
							<td>{{ $user->firstname }}</td>
							<td>{{ $user->lastname }}</td>
							<td>{{ \Carbon\Carbon::parse($user->geboortedatum)->format('d-m-Y') }}</td>
							<td>{{ $user->email }}</td>
						<td>
							<a href="/users/{{ $user->id }}/restore"><button type="button" class="btn btn-success">Herstellen</button></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
@stop