@extends('welcome')

@section('content')
@include('/errors._form_errors')
	<div class="col-md-9 col-md-offset-1">
		<form method="post" action="/users/{{ $user->id }}/update">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group">
		  			<input type="text" class="form-control" name="firstname" value="{{ $user->firstname }}" minlength="1" maxlength="45" required/>
		  		</div>
		  		<div class="form-group">
		  			<input type="text" name="lastname" value="{{ $user->lastname }}" class="form-control" minlength="1" maxlength="45" required/>
		  		</div>
		  		<div class="form-group">
		  			<input type="date" name="geboortedatum" value="{{ $user->geboortedatum }}" class="form-control" />
		  		</div>
		  		<div class="form-group">
		  			<input type="text" name="email" value="{{ $user->email }}" class="form-control" minlength="1" maxlength="120" required/>
		  		</div>

			  	<div class="form-group">
			  		<button type="submit" class="btn btn-success form-control">Wijzigen</button>
			  	</div>
			</form>
	</div>
@stop