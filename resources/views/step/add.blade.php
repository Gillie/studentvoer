@extends('welcome')

@section('content')
@include('/errors._form_errors')
	<div class="col-md-9 col-md-offset-1">
		<form method="post" action="/recipes/{{ $id }}/steps/add">
				{{ csrf_field() }}
		  		<div class="form-group">
		  			<textarea name="step" class="form-control" placeholder="Stap" rows="4">{{ old('step') }}</textarea>
		  		</div>
			  	
			  	<div class="form-group">
			  		<button type="submit" class="btn btn-success form-control">Toevoegen</button>
			  	</div>
			</form>
	</div>
@stop