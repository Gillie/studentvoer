@extends('welcome')

@section('content')
@include('/errors._form_errors')
	<div class="col-md-9 col-md-offset-1">
		<form method="post" action="/recipes/{{ $id }}/steps/{{ $step->id }}/update">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
		  		<div class="form-group">
		  			<textarea name="step" class="form-control" placeholder="Stap" rows="4">{{ $step->step }}</textarea>
		  		</div>
			  	
			  	<div class="form-group">
			  		<button type="submit" class="btn btn-success form-control">Wijzigen</button>
			  	</div>
			</form>
	</div>
@stop